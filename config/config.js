var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'savings'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://system_admin:1234Abc!@ds139879.mlab.com:39879/xpense'
  },

  test: {
    root: rootPath,
    app: {
      name: 'savings'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/savings-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'savings'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/savings-production'
  }
};

module.exports = config[env];
