'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TransactionSchema = new Schema({
	amount: {
		type: Number,
		required: true,
		default: 0.0
	},
	transactionType: {
		type: String
	},
	account: {
		type: Schema.ObjectId,
		ref: 'Account'
	}
});

mongoose.model('Transaction', TransactionSchema);