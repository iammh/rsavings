// Example model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Transaction = mongoose.model('Transaction'),
  Async = require('async');

var AccountSchema = new Schema({
  title: {
  	type: String,
  	unique: true
  }
});

AccountSchema.virtual('date')
  .get(function(){
    return this._id.getTimestamp();
  });

 AccountSchema.statics.balance = function (account,callback) {

 	var balance = 0;
 	Transaction.find({account: account}, function (err, transactions) {
 		if(!err) {
 			Async.eachSeries(transactions, function (transaction,next) {
 				if(transaction.transactionType !== 'D') {
					balance -= Number(transaction.amount);
 				} else {
 					balance += Number(transaction.amount);
 				}
 				next();
 				
 			}, function done() {
 				callback(null,balance);
 			});
 		}
 		else {
 			callback(err,null);
 		}
 	})
 }

mongoose.model('Account', AccountSchema);

