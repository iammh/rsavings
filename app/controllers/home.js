var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Account = mongoose.model('Account'),
  Transaction = mongoose.model('Transaction');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
  Account.find(function (err, articles) {
    if (err) return next(err);
    res.render('index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
  });
});


  router
    .route('/account')
    .get(function (req,res,next) {
      Account.balance('58988fe7c232562ad8391ef0', function (err,balance) {
        res.json({data: balance})
      });
    });
  router
    .route('/account/create')
    .post(function (req,res,next) {
      var body = req.body;

      var account = new Account(body);
      account.save(function (err,savedInstance) {
        res.json(savedInstance);
      });
    });


  router
    .route('/transaction/create')
    .post(function (req,res,next) {
      var body = req.body;

      var transaction = new Transaction(body);
      transaction.save(function (err,savedInstance) {
        res.json(savedInstance);
      });
    });